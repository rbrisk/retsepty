# Deruny (деруни)

## Zutaten

- 500 ml Milch 
- 300 g Weizenmehl
- 300 g Ruchmehl
- 3-4 (ca. 300-400 g) Äpfel 
- 2-3 (ca. 200-300 g) Rüebli
- 2 Eier
- 3-4 Prisen Salz
- 2-3 TL Backpulver
- 1-2 EL Öl (z.b. Rapsöl oder Olivenöl)
- Dillspitzen, Pfeffer nach Geschmack

## Zubereitung

Rüebli fein reiben.

Äpfel in Viertel schneiden, die Kerne entfernen, schalen und grob reiben.

Weizenmehl, Ruchmehl und Backpulver in eine Schüssel geben, mischen. Alle anderen Zutaten ausser der Milch beigeben. Die Milch in kleinen Mengen hinzufügen und dabei mit einem Löffel umrühren. Der Teig sollte die Konsistenz eines dickflüssigen Joghurts haben. Wasser in kleinen Mengen beigeben falls er zu dickflüssig ist.

Den Teig in der Schüssel ruhen lassen, bis die Pfanne heiß ist (ca. 10 Minuten).

Je Pfannkuchen ca. 2 EL Teig in die Pfanne geben und flach drücken. Wenn der Oberfläche meistens trocken ist, den Pfannkuchen wenden und auch von der anderen Seite goldbraun backen.